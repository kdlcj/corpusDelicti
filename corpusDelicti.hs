import Codec.Binary.UTF8.String
import Network.HTTP
import Control.Monad
import System.IO
import System.Environment

type URL = String
data Date = Date { day :: String, month :: String, year :: String } deriving Show

--  the first and the last day of a year, used to split 
--  long intervals into a shorter, year-long ones
firstday :: Date
firstday = Date { day = "01", month = "01", year = "" }

lastday :: Date
lastday  = Date { day = "31", month = "12", year = "" }

baseUrl :: URL
baseUrl = "http://www.nsoud.cz/"

resultUrlPart :: URL
resultUrlPart = "JudikaturaNS_new/judikatura_vks.nsf/WebSearch/"

--  downloads page as html text via http
downloadhtml :: URL -> IO String
downloadhtml url 
    =  putStr "." >> hFlush stdout 
    >> simpleHTTP (getRequest url) >>= \x -> liftM decodeString (getResponseBody x)

--  splits date into year-long intervals - needed to process queries 
--  with number of results exceeding one thousand (1000),
--  the number being the internal website size limit
years :: (Date, Date) -> [(Date, Date)]
years (from, to) = 
    if   (1 + (read $ year from :: Int)) < (read $ year to :: Int)
    then (from, lastday { year = year from }) : otherTimes
    else [(from, to)]
    where 
    render stringyYear = show $ succ (read stringyYear :: Int)
    otherTimes = years (firstday { year = render $ year from }, to)

--  creates url of search page which contains links 
--  to relevant page containing the data itself
composeUrl :: (Date, Date) -> URL
composeUrl (from, to)
    =  baseUrl ++ "JudikaturaNS_new/judikatura_vks.nsf/"
    ++ "$$WebSearch1?SearchView&Query=%5Bdatum_rozhodnuti%5D%3E%3D"
    ++ day from ++ "%2F" ++ month from ++ "%2F" ++ year from 
    ++ "%20AND%20%5Bdatum_rozhodnuti%5D%3C%3D" ++ day to ++ "%2F"
    ++ month to ++ "%2F" ++ year to 
    ++ "&SearchMax=0Start=1&Count=9999&pohled=1&searchOrder=4"

--  finds substrings in text which are ended with >"< (quotation mark) 
--  terminating a specific html value
finder :: String -> String -> [String]
finder _ []             = []                    
finder what wher@(_:r)  = 
    if   take (length what) wher == what
    then takeWhile ( /= '\"' ) wher : finder what r
    else finder what r

--  extracts links from the search page pointiing to the data into a list
links :: (Date, Date) -> IO [URL]
links (from, to) = liftM (map (baseUrl ++) 
                 . finder resultUrlPart) (downloadhtml $ composeUrl (from, to))

--  extracts links as well, but uses list of time intervals
linklist :: (Date, Date) -> IO [URL]
linklist (from, to) = fmap concat $ sequence 
                    $ foldr ( \x y -> links x : y ) [] (years (from, to))

--  extracts actual text from html file using empirical html delimiter
extract :: String -> String
extract = removeinitws . removebs 
        . removenbsp . removetags . cutout "<hr class=\"line\">"

--  remove leading whitespace
removeinitws :: String -> String
removeinitws ""    = ""
removeinitws (w:r) = if w == '\n' || w == ' ' || w == '\t' then removeinitws r else w:r

--  removes tab characters, spaces after new line,
--  and consecutive spaces and newline charactes
removebs :: String -> String
removebs ""                  = ""
removebs "\n"                = ""
removebs [x]                 = [x]
removebs (x:y:r)
    | x == '\n' && y == '\n' = removebs (x:r)
    | y == '\t'              = removebs (x:' ':r)
    | x == '\n' && y == ' '  = removebs (x:r)
    | x == ' '  && y == ' '  = removebs (x:r)
    | otherwise              = x : removebs (y:r)

--  removes non-breaking space html tags and such
removenbsp :: String -> String
removenbsp ""                  = ""
removenbsp ('&':_:_:_:_:';':r) = removenbsp r
removenbsp ('&':_:_:_:';':r)   = removenbsp r
removenbsp ('&':_:_:';':r)     = removenbsp r
removenbsp (x:xs)              = x : removenbsp xs

--  cuts out the begining of a text until a given substring is detected
cutout :: String -> String -> String
cutout _ ""            = ""
cutout what wher@(_:r) = 
    if   take (length what) wher == what
    then wher
    else cutout what r

--  removes html tags
removetags :: String -> String
removetags = removeTags False
    where
    removeTags reading (x:xs) 
        | not reading && x /= '<' = x : removeTags reading xs
        | reading && x /= '>'     = removeTags reading xs
        | x == '<'                = removeTags True xs
        | x == '>'                = removeTags False xs
        | otherwise               = removeTags False (x:xs)
    removeTags _ ""               = []

--  downloads data and writes them into a file
downWrite :: FilePath -> [URL] -> IO ()
downWrite fp = foldr (\x y -> downloadhtml x >>= appendFile fp . document x >> y)
             $ return ()

--  separates one downloaded page using xml tags
document :: URL -> String -> String
document url string =  "<file url=\"" ++ url 
                    ++ "\">\n" ++ extract string ++ "\n</file>\n"

--  converts string to date
readDate :: String -> Date
readDate (d1:d2:'-':m1:m2:'-':y1:y2:y3:y4:_) 
    = Date { day = [d1,d2], month = [m1,m2], year = [y1,y2,y3,y4] }
readDate _ = error errorMessage

--  inserts meta information to the beginning of the file
generatemeta :: (Date, Date) -> FilePath -> Int -> IO ()
generatemeta (from, to) fp size 
    =  appendFile fp 
    $  "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
    ++ "<doc count=\""
    ++ show size
    ++ "\" from=\"" ++ year from ++ "-"++ month from ++ "-" ++ day from
    ++ "\" to=\"" ++ year to ++ "-" ++ month to ++ "-" ++ day to
    ++ "\">\n"

--  message displayed when number of cl arguments is not 3
errorMessage :: String
errorMessage = "Usage: corpusDelicti [FILENAME] [DD-MM-YYYY] [DD-MM-YYYY]"

main :: IO ()
main = do
    args <- getArgs
    if length args /= 3 then putStrLn errorMessage else do
    let fpath = head args
    let from = readDate $ args !! 1
    let to = readDate $ args !! 2
    list <- linklist (from, to)
    generatemeta (from, to) fpath $ length list
    downWrite fpath list
    appendFile fpath "</doc>\n"
    putStrLn $ "\nok, " ++ show (length list) ++ " downloaded pages."

