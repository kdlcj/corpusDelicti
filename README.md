CorpusDelicti
======

### CorpusDelicti is a generator of a text corpora of juducial decisions from website "nsoud.cz" in a time interval specified as arguments into a file

#### Compilation 
program is compiled as ``ghc CorpusDelicti.hs``. This program needs installed libraries ``http`` and ``utf8-string``
available for example via package manager ``cabal``.

#### Use
program accepts three parameters - the first one is the name of file to be created, the second is a date, from which
the entries are going to be downloaded, and the third one is a date until which the files are going to be downloaded.
The dates are to be entered in form ``DD-MM-YYYY``.
